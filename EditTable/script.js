class TableObject
{
    constructor(id,name,surname,age,birthdate)
    {
        this.Id = id;
        this.Name = name;
        this.Age = age;
        this.Surname = surname;
        this.Birthdate = birthdate;
    }
    set Id(value){
        this._id = value;
    }
    get Id(){
        return _id;
    }
    get Name(){
        return _name;
    }
    set Name(value){
       this. _name = value;
    }
    get Surname(){
        return _surname;
    }
    set Surname(value){
       this. _surname = value;
    }
    get Age(){
        return _age;
    }
    set Age(value){
        this._age = value;
    }
    set BirthDate(value){
        this._birthdate = value;
    }
    get BirthDate(){
        return _birthdate;
    }
}

let RowList = new Array();//Массив строк таблицы 
let TableIdName = "MainTable";

RowList.push(new TableObject(1,"John","Carter",29,"12/21/39"));
RowList.push(new TableObject(1,"John","Carter",29,"12/21/39"));

function CreateTable(TableId)//Создаю таблицу на основе RowList
{
    let table = document.getElementById(TableId);
    for(let custom of RowList)
    {
        let tr = document.createElement('tr');
        
        

        for(let key in custom)
        {
            let td  = document.createElement('td');
            td.innerHTML = custom[key]+"";
            tr.appendChild(td);
        }

        table.appendChild(tr);
    }
}

function AppendTable()
{
    let Child = new TableObject(1,"John","Carter",29,"12/21/39");
    RowList.push(Child);
    let table = document.getElementById(TableIdName);
    let tr = document.createElement('tr'); 
    for(let key in Child)
    {
        let td  = document.createElement('td');
        td.innerHTML = Child[key]+"";
        tr.appendChild(td);
    }
    let button = document.createElement('button');
    button.innerHTML = "remove";
    button.style.width ="100%";
    button.style.height="100%";
    button.className = "btn btn-outline-danger";
    tr.appendChild(document.createElement('td').appendChild(button));
    table.appendChild(tr);
}

function RemoveRow(target)
{
    target.parentNode.remove();
}
//document.onload = CreateTable(TableIdName);
function MakeInput(target)
{
    let TdValue = target.innerHTML;

    target.innerHTML = '';
    let input = document.createElement('input');
    input.value = TdValue;
    input.className ="Inputs";
    

    target.appendChild(input);
}
document.getElementById("AddButton").onclick = function(){
    AppendTable();
}


document.getElementById("MainTable").onclick = function(event)
{
    let target = event.target;
    if(target.className ==="btn btn-outline-danger")
    {
        RemoveRow(target);
    }
    MakeInput(target);
}


